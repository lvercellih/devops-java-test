package lvh.example.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloRestController {

    @RequestMapping
    public Map<String, String> index(HttpServletRequest request) {
        Map<String, String> result = new HashMap<>();
        Enumeration<String> headerNamesEnumeration = request.getHeaderNames();
        while (headerNamesEnumeration.hasMoreElements()) {
            String headerName = headerNamesEnumeration.nextElement();
            result.put(headerName, request.getHeader(headerName));
        }
        return result;
    }
}
